package com.tdd.authorization;

import java.util.ArrayList;

public class LoginService {

    public boolean loginReturnBoolean(String username, String password, ArrayList<User> users) {
       return users.stream().anyMatch(user -> user.getUsername()
               .equalsIgnoreCase(username) && user.getPassword().equals(password));
    }

    public Token loginReturnToken(String username, String password, ArrayList<User> users) throws UserNotFoundException {
        if(users.stream().anyMatch(user -> user.getUsername()
                .equalsIgnoreCase(username) && user.getPassword().equals(password))) {
            return new Token(username);
        } else if(users.stream().anyMatch(user -> user.getUsername()
                .equalsIgnoreCase(username))) {
            throw new UserNotFoundException("Incorrect Password");
        } else if(users.stream().anyMatch(user -> user.getPassword()
                .equals(password))) {
            throw new UserNotFoundException("Incorrect Username");
        } else {
            throw new UserNotFoundException("Incorrect username and password");
        }
    }
}
