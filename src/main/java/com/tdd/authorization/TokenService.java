package com.tdd.authorization;

import java.util.ArrayList;

public class TokenService {
    ArrayList<Token> tokens = new ArrayList<>();

    public boolean validateToken(Token token) throws Exception {
        if(tokens.contains(token)){
            return true;
        } else throw new Exception("token not present.");
    }

    public Token createToken(String username) {
        Token token = new Token(username);
        tokens.add(token);
        return token;
    }

//    public void tokenList() {
//        tokens.add(new Token("Addis"));
//        tokens.add(new Token("Shew"));
//        tokens.add(new Token("Dire"));
//    }


}

