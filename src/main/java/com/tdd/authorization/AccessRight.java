package com.tdd.authorization;

public enum AccessRight {
    READ,
    WRITE,
    EXECUTE
}
