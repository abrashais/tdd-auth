package com.tdd.authorization;

import lombok.Data;

import java.util.UUID;

@Data
public class User {

    String id;
    String username;
    String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.id = UUID.randomUUID().toString();
    }
}
