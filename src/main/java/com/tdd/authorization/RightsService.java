package com.tdd.authorization;

import java.util.ArrayList;
import java.util.List;

public class RightsService {


    public UserRight getUseRight(String username, String resource) throws Exception {
        List<AccessRight> accessRights = new ArrayList<>();
        if(username.equals("berit") && resource.equals("ACCOUNT")) {
            accessRights.add(AccessRight.READ);
            accessRights.add(AccessRight.WRITE);
            return new UserRight(username, resource, accessRights);
        } else if(username.equals("anna") && resource.equals("ACCOUNT")) {
            accessRights.add(AccessRight.READ);
            return new UserRight(username, resource, accessRights);
        } else if(username.equals("kalle") && resource.equals("PROVISION_CALC")) {
            accessRights.add(AccessRight.EXECUTE);
            return new UserRight(username, resource, accessRights);
        } else throw new Exception("username not found OR wrong resource");
    }
}
