package com.tdd.authorization;

import lombok.Data;

import java.util.UUID;

@Data
public class Token {

    String id;
    String username;

    public Token(String username) {
        this.username = username;
        this.id = UUID.randomUUID().toString();
    }
}