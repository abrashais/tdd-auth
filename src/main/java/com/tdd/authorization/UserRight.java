package com.tdd.authorization;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class UserRight {

    String username;
    String resource;
    List<AccessRight> accessRights;


}
