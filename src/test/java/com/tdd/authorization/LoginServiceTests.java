package com.tdd.authorization;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class LoginServiceTests {

    LoginService loginService = new LoginService();
    ArrayList<User> users = new ArrayList<>();
    PasswordEncryptor passwordEncryptor = new PasswordEncryptor();
    RightsService rightsService = new RightsService();
    String resource;
    String username;

    @BeforeEach
    void setUp() {
        userList();
        resource = "ACCOUNT";
        username = "anna";
    }
    @Test
    void ifUser_is_inTheList_thenLogin_returnsTure() {
        boolean user = loginService.loginReturnBoolean("anna", "losen", users);
        assertThat(user, is(true));
    }

    @Test
    void isPassword_encrypted() {
        String encrypted = passwordEncryptor.passwordEncryptor("password");
        assertThat(encrypted, is("5f4dcc3b5aa765d61d8327deb882cf99"));

    }

    @Test
    void getUser_right_forSpecific_resource() throws Exception {
        UserRight result = rightsService.getUseRight(username, resource);
        assertThat(result.getAccessRights(), is(notNullValue()));
    }

    public void userList() {
        users.add(new User("anna", "losen"));
        users.add(new User("berit", "123456"));
        users.add(new User("kalle", "password"));

    }

}
/*  accessRight(Token token, String resource) {

    }

*/