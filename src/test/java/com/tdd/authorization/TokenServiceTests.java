package com.tdd.authorization;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TokenServiceTests {

    LoginService loginService = new LoginService();
    ArrayList<User> users = new ArrayList<>();
    PasswordEncryptor passwordEncryptor = new PasswordEncryptor();

    TokenService tokenService = new TokenService();
    String username;
    @BeforeEach
    void setUp() {
        userList();
            username = "anna";

    }
    @Test
    void ifUser_is_inTheList_thenLogin_returnToken() throws UserNotFoundException {
        Token result = loginService.loginReturnToken("anna", "losen", users);
        assertThat(result, is(notNullValue()));
    }

    @Test
    void if_user_isNot_inTheList_thenLogin_throwsAnException() {
        Assertions.assertThrows(UserNotFoundException.class, () ->
                loginService.loginReturnToken("anna", "losen", users));
    }

    @Test
    void validateToken() throws Exception {
        Token token = tokenService.createToken(username);
        boolean result = tokenService.validateToken(token);
        assertThat(result, is(true));
    }

    public void userList() {
        users.add(new User("anna", "losen"));
        users.add(new User("berit", "123456"));
        users.add(new User("kalle", "password"));
    }

}